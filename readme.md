# **PROJETO DE ESTUDO DA ALURA**


## Tecnologias

 -  `PHP version 7.3.12`
 -  `Composer version 1.9.1`
 -  `Laravel 5.8.*`
 
### **Instalando o projeto**

 1. Faça download do PHP, extraia a sua pasta e insira o caminho dela em Path como uma variável de ambiente;
 2. Abra a pasta que você extraiu e procure se existe algum arquivo com a nomenclatura `php.ini`. Se o arquivo não existir, copie e cole o arquivo `php.ini-development` e o renomeie para php.ini. Abra o arquivo e verifique se existe uma linha com o seguinte comando `extension=pdo_sqlite`. Se ela possuir um `;`na frente, remova-o. Se não possuir esse comando, insira.
 2. Faça download do instalador do composer para windows e o execute;
 3. abra o projeto e insira o comando ``composer install`` no terminal (se utilizar o PHPStorm) ou no cmd dentro da pasta do projeto  para que todos os arquivos necessários e dependências sejam baixados;
 4. Execute o comando `php artisan key:genereate` para adicionar uma string à linha APP_KEY do arquivo `.env`; 
 5. No projeto, no diretório `projeto\database\` crie um arquivo `database.sqlite`
 6. Após a instalação desses arquivos, execute o comando `php artisan migrate` para que as migrations que foram criadas sejam executadas
 7. Por fim, execute o comando `php artisan serve` para que o projeto seja executado. Será informado o host do server, copie e cole no navegador para acessar a aplicação.
 
 
 #### Erro  500 | Server Error
 
 Caso o erro 500 | Server Error persista, execute o comando na pasta do projeto via cmd:
 
    copy .env.example .env

  Se o arquivo `.env` não existir, e em seguida execute:
  
    php artisan key:generate
